function iniciar() {
var boton = document.getElementById("validar");
if(boton.addEventListener){
boton.addEventListener("click", validar, false);
}
else if(boton.attachEvent){
boton.attachEvent("onclick", validar);
}
}
function validar(){
var tipo = document.formulario.seleccionartipo.options[formulario.seleccionartipo.selectedIndex].value;
var dato = document.formulario.txtdato.value;
var valido = false;
var resultado = null;
//alert(dato + "|" + tipo);
if(dato == null || dato == "" || dato.length == 0){
alert("No se ha ingresado ningún valor en el campo de formulario");
return 0;
}
switch(tipo){

case "pasa":
resultado = /^[A-Z]{1}[0-9]{8}$/;
if(resultado.test(dato)){
valido = true;
}
break;

case "tel":
resultado = /^[2,7,6]{1}[0-9]{7}$/;
if(resultado.test(dato)){
valido = true;
}
break;

case "car":
resultado = /^[A-Z]{2}[0-9]{6}$/;
if(resultado.test(dato)){
valido = true;
}
break;

case "cdb":
resultado = /^([a-zA-Z0-9_\.\-])+\@(([cdb.edu.sv])+\.)+([cdb.edu.sv]{2,4})+$/;
if(resultado.test(dato)){
	valido = true;
}
break;

case "Google":
resultado = /^([a-zA-Z0-9_\.\-])+\@(([gmail.com,yahoo.com,outlook.com,.es])+\.)+([gmail.com,yahoo.com,outlook.com,.es]{2,4})+$/;
if(resultado.test(dato)){
	valido = true;
}

break;
case "imag":
resultado = /^[0-9]{1,2}[+]{1}[0-9]{1,2}[i]{1}$/;
if(resultado.test(dato)){
	valido = true;
}
break;

default:
alert("Intenta de nuevo");
break;
}
if(valido == true){
alert("El valor " + dato + " ingresado es válido");
}
else if(valido == false){
alert("El valor " + dato + " ingresado es inválido");
}
}
if(window.addEventListener){
window.addEventListener("load", iniciar, false);
}
else if(window.attachEvent){
window.attachEvent("onload", iniciar);
}